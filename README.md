# Movies App - React App (Platform Electives Objective)
A simple React App that allows users to add movies and their details through a Form. These movies and their data would then be stored through a backend database, which in this case is Firebase. Title, Opening Text, and Release Date are the input fields needed for each movie. A button to fetch all the stored data in the database is also included which reads the saved data by the user through the form.

## Setup

```
npm install
npm run start
```
